#include "LoRaRadio.h"

LoRaRadio::LoRaRadio(String call) :
  call(call)
//  disp(0)
{
};

LoRaRadio::LoRaRadio(String call, Display &disp) :
  call(call),
  disp(disp)
{
};

void LoRaRadio::init() {
  // override the default CS, reset, and IRQ pins (optional)
  LoRa.setPins(LORA_CS_PIN, LORA_RESET_PIN, LORA_IRQ_PIN); // set CS, reset, IRQ pin

  if (!LoRa.begin(this->LoRaFreq)) {             // initialize ratio at 915 MHz
    Serial.println("LoRa init failed. Check your connections.");
    while (true);                       // if failed, do nothing
  }

  LoRa.setFrequency(this->LoRaFreq);                // Change the frequency of the radio.
  LoRa.setSpreadingFactor(this->LoRaSpreadingF);    // ranges from 6-12,default 7 see API docs
  LoRa.setSignalBandwidth(this->LoRaBw);            // Supported values are 7.8E3, 10.4E3, 15.6E3, 20.8E3, 31.25E3, 41.7E3, 62.5E3, 125E3, 250E3, and 500E3. defaults to 125E3
  LoRa.setCodingRate4(this->LoRaCodingRate4);       // Supported values are between 5 and 8,defaults to 5
  LoRa.setPreambleLength(this->LoRaPreambleLength); // Supported values are between 6 and 65535. defaults to 8
  LoRa.setSyncWord(this->LoRaSyncWord);             // byte value to use as the sync word, defaults to 0x12

  LoRa.disableCrc();  //LoRa.enableCrc() Enable or disable CRC usage, by default a CRC is not used.
  LoRa.disableInvertIQ();  // LoRa.enableInvertIQ();Enable or disable Invert the LoRa I and Q signals, by default a invertIQ is not used.

  LoRa.setTxPower(this->LoRaPower); // TX power in dB, defaults to 17

  // parse for a packet, and call onReceive with the result:
  //  LoRa.onReceive(parsePacket);
  LoRa.receive();

  Serial.println("LoRa init succeeded.");
  if (LoRaModeDirect) {
    Serial.println("Direct mode. Frequency: " + String(LoRaFreq / 1e3) + " kHz");
  } else {
    Serial.println("Using repeater. RX: " + String(LoRaFreq / 1e3) + " TX: " + String(LoRaFreqTx / 1e3) + " Pwr: " + String(LoRaPower));
  }
  Serial.println("Bandwidth: " + String(LoRaBw / 1e3) + " kHz");
  Serial.println("Spreading factor: " + String(LoRaSpreadingF));
  Serial.println("LoRaCodingRate4: " + String(LoRaCodingRate4));
  Serial.println("============================================");
  RxMessage inMsg;
}

void LoRaRadio::pollRx() {
  int packetSize = LoRa.parsePacket();
  if (packetSize == 0) return;          // if there's no packet, return
  parsePacket(packetSize);
}

RxMessage* LoRaRadio::parsePacket(int packetSize) {
  if (packetSize == 0) return NULL;          // if there's no packet, return

  // read packet header bytes:
  String incoming = "";
  while (LoRa.available()) {
    incoming += (char)LoRa.read();
  }
//  RxMessage inMsg(incoming);
  inMsg.parseMessage(String(incoming));
  inMsg.rssi = LoRa.packetRssi();
  inMsg.snr = LoRa.packetSnr();
  inMsg.freqOffset = LoRa.packetFrequencyError();
  //  newMessageReceived = True;
//  if (disp) { //testing: todo: add check!
//    disp.displayRxMessage(inMsg);
//  }
  Serial.println("LoRa Rx: " + incoming);
  return &inMsg;
}

RxMessage* LoRaRadio::loop() {
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    parsePacket(packetSize);
  }
  return &inMsg;
}

void LoRaRadio::send(Message msg) {
  String outgoing = "[" + msg.source + ":] " + String(msg.message);
  if (msg.frequency <= 0) {
    if (LoRaModeDirect) {
      msg.frequency = LoRaFreq;
    } else {
      msg.frequency = LoRaFreqTx;
    }
  }
  LoRa.setFrequency(msg.frequency);
  LoRa.beginPacket();                   // start packet
  LoRa.print(outgoing);                 // add payload
  LoRa.endPacket();                     // finish packet and send it
  LoRa.setFrequency(LoRaFreq);
  LoRa.receive();
  Serial.println(outgoing);
  Serial.println("Tx Freq: " + String(msg.frequency / 1e3) + " kHz");
}

void LoRaRadio::setDisplay(Display &disp) {
  this->disp = disp;
}
