#ifndef LRH_DISPLAY_H
#define LRH_DISPLAY_H

//system includes
#include <Arduino.h>
//libraries to be installed
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>


#include "Message.h"
#include "RxMessage.h"

// Screendefinition is importend on ESP32 because without you get Kernelpanic
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1 // should be set to -1 if there is no reset pin

//screen coordinates
#define FIRSTLINE  16
#define FONTSIZE   7
#define LINEHEIGHT 8
#define STATUSAREA 40

class Display {

  public:
    //constructor
    Display();

    //functions
    void init();
    void displayTx(bool sending);
    void displayTxMessage(Message msg);
    void displayRxMessage(RxMessage incoming);
    void displayString(String in);

  private:
    // variables
    Adafruit_SSD1306 mydisplay;
    bool sending = false;

    //functions
};

#endif
