#include "Message.h"

Message::Message() {
  this->message;
  this->source;
  this->destination;
  this->timestamp = 0;
  this->frequency = 0;
  this->processed = false;
}

Message::Message(String call) {
  this->message = "";
  this->source = call;
  this->destination;
  this->timestamp = 0;
  this->frequency = 0;
  this->processed = false;
}

Message::Message(String call, String message) {
  this->message = message;
  this->source = call;
  this->destination = "";
  this->timestamp = 0;
  this->frequency = 0;
  this->processed = false;
//  Serial.println("Message created: " + this->getFullString());
}

//Message::Message(String call, String message, const int frequency) {
//  this->message = message;
//  this->source = call;
//  this->destination = [];
//  this->timestamp = 0;
//  this->frequency = frequency;
//  this->processed = false;
//}

String Message::getFullString() {
  Serial.println("Message::getFullString() called");
  String outString = "";
  if (destination == "") {
    outString = source + ": " + message;
  } else {
    outString = source + ": " + destination + ">" + message;
  }
  return outString;
}
