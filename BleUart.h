#ifndef LRH_BLEUART_H
#define LRH_BLEUART_H

// system includes
#include <Arduino.h>
// BLE libraries to be installed with the library manager
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

// Project includes
#include "Message.h"
#include "Display.h"
#include "LoRaRadio.h"

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

class BleUart {

  public:
    //constructor
    BleUart();

    //functions
    void loop();
    void bleSetup(String call);
    void bleForwardMessage(Message *toBle);
    void setLoRaController(LoRaRadio *radio);
    void setDisplay(Display *disp);

    //variables
    bool deviceConnected = false;

  private:
    //functions
    
    //variables
    BLEServer *pServer = NULL;
    BLECharacteristic * pTxCharacteristic;
    bool oldDeviceConnected = false;
    uint8_t txValue = 0;
    String deviceName = "";

    //references to other controllers
    Display* _disp = NULL;
    LoRaRadio* _radio = NULL;
};

#endif
