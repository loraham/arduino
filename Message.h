#ifndef LRH_MESSAGE
#define LRH_MESSAGE

#include <Arduino.h>

class Message {
  public:
    //constructor
    Message();
    Message(String call);
    Message(String call, String message);
//    Message(String call, String message, unsigned int frequency);

    //functions
    String getFullString();

    //variables
    String message;
    String source;
    String destination;
    unsigned int timestamp;
    unsigned int frequency;
    bool processed;
};

#endif
