/*

  This code is designed for Gualtherius LoRaHAM. Pinout and PCB for 1 Watt of freedom.

  here comes some cool text in future.

  IDE  : Arduino 1.8.12
  Lib  : arduino-LoRa 0.7.2 by sandeepmistry
       : Adafruit_SSD1306 2.4.0
       : Adafruit-GFX-Library 1.10.2
       :
  Board: Gualtherius LoRaHAM ESP32 V1.00

  created 04.11.2020
  by Alexander Walter

  based on LoRa from Tom Igoe

  https://github.com/sandeepmistry/arduino-LoRa/blob/master/API.md
*/

//#include "OTA.h"
#include "credentials.h"
#include "Display.h"
#include "Message.h"
#include "RxMessage.h"
#include "LoRaRadio.h"
#include "BleUart.h"


// auto-messaging
byte msgCount = 0;            // count of outgoing messages
unsigned int interval = 120000;          // interval between sends
long nextBeacon = 0;
bool newMessageReceived = 0;  // bit indicating whether a new message (i.e. not sent via BLE) is received
String message = "CQ JN58ak";
String serialMessage = "";

String call = "DK2FK-1";

Display myDisplay;
LoRaRadio myRadio(call);
//LoRaRadio myRadio(call, myDisplay);
BleUart bleUart{};
Message txMsg(call);

void setup() {
  Serial.begin(115200);                   // initialize serial
  while (!Serial);
  //  /setupOTA("Gualtherius LoRaHAM JW34", mySSID, myPASSWORD);
  myDisplay.init();
  myRadio.init();
  myRadio.setDisplay(myDisplay);
  bleUart.bleSetup(call);
}

void loop() {
  RxMessage *msg = myRadio.loop();
  if (!msg->processed) {
    Serial.println("Forwarding message received on LoRa to BLE");
    myDisplay.displayRxMessage(*msg);
    bleUart.bleForwardMessage(msg);
    msg->processed = true;
  }
  if (millis() > nextBeacon) {
    String outMessage = message + " " + msgCount;
    Message msg(call, outMessage);
    sendMessage(msg);
    nextBeacon = millis()  + interval + random(round(interval / 5));  // interval + max 20% of configured value
    msgCount++;
  }
//  bleLoop();
  bleUart.loop();
  serialLoop();
}

void sendMessage(Message msg) {
  myDisplay.displayTx(true);
  myRadio.send(msg);
  myDisplay.displayTxMessage(msg);
  bleUart.bleForwardMessage(&msg);
  msg.processed = true;
}
