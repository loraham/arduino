#include "RxMessage.h"

RxMessage::RxMessage() {
  snr = 0;
  rssi = 0;
  freqOffset = 0;
  message = "";
  processed = false;
}

RxMessage::RxMessage(String incoming) {
  parseMessage(incoming);
  processed = false;
}

void RxMessage::parseMessage(String incoming) {
  int startIndex = 0;
  if (incoming.charAt(0) == '[') {
    startIndex = 1;
  }
  int callSeparator = incoming.indexOf("]");
  if (callSeparator > 3 && callSeparator < 50) {
    if (incoming.charAt(callSeparator-1) == ':') {
      this->source = incoming.substring(startIndex, callSeparator-1);
    } else {      
      this->source = incoming.substring(startIndex, callSeparator);
    }
    if (incoming.charAt(callSeparator + 2) == ' ') {
      this->message = incoming.substring(callSeparator + 1);
    } else {
      this->message = incoming.substring(callSeparator + 2);
    }
  } else {
    this->source = "";
    this->message = incoming;
  }

  int forwardingIndex = incoming.indexOf(">");
  if (forwardingIndex > 0) {
    if (callSeparator > 0) {
      this->destination = incoming.substring(callSeparator + 2, forwardingIndex);
      this->message = incoming.substring(callSeparator + 2);
    } else {
      this->destination = incoming.substring(0, forwardingIndex);
      this->message = incoming.substring(0);
    }
  }
  processed = false;
}
