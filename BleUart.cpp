#include "BleUart.h"



class MyServerCallbacks: public BLEServerCallbacks {

  private:
    BleUart* _ctx;

  public:
    MyServerCallbacks(BleUart* ctx) : _ctx{ctx} {

    };

    void onConnect(BLEServer * pServer) {
      _ctx->deviceConnected = true;
      Serial.println("BLE connected");
    };

    void onDisconnect(BLEServer * pServer) {
      _ctx->deviceConnected = false;
      Serial.println("BLE disconnected");
    };
};

class MyCallbacks: public BLECharacteristicCallbacks {


    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      //      String input;
      if (rxValue.length() > 0) {
        Serial.print("BLE in: ");
        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]);
          //          serialMessage += (char)rxValue[i];
        }
        //        Serial.println(" [" + String(rxValue.length()) + "]");
        //        Serial.println(serialMessage);
        //        Message msg(call, serialMessage);
        //        //        myRadio.send(msg);
        //        serialMessage = "";
      }
    }
};


BleUart::BleUart()
{
}

void BleUart::setLoRaController(LoRaRadio *radio) {
  _radio = radio;
}

void BleUart::setDisplay(Display *disp) {
  _disp = disp;
}

void BleUart::bleSetup(String call) {
  //  Serial.begin(115200);
  
  // Create the BLE Device
  deviceName = "LoRaHam " + call; 
  BLEDevice::init(deviceName.c_str());

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks(this));

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                        CHARACTERISTIC_UUID_TX,
                        BLECharacteristic::PROPERTY_NOTIFY
                      );

  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
      CHARACTERISTIC_UUID_RX,
      BLECharacteristic::PROPERTY_WRITE
                                          );

  pRxCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}

// forward message received on LoRa to connected BLE device
void BleUart::bleForwardMessage(Message *toBle) {
  if (deviceConnected) {
    String fromLora = toBle->getFullString();
    Serial.println("BLE Tx: " + fromLora);
    char charstring[21];
    int length = fromLora.length();
    int sentIndex = 0;
    while ((length - sentIndex) > 20) {
      fromLora.substring(sentIndex, sentIndex + 20).toCharArray(charstring, 20 + 1);
      sentIndex += 20;
      pTxCharacteristic->setValue(charstring);
      pTxCharacteristic->notify();
    }
    fromLora.substring(sentIndex, length).toCharArray(charstring, length - sentIndex + 1);
    pTxCharacteristic->setValue(charstring);
    pTxCharacteristic->notify();
  } else {
    Serial.println("No BLE device connected");
  }
}

void BleUart::loop() {

  //  if (deviceConnected) {
  //    pTxCharacteristic->setValue(&txValue, 1);
  //    pTxCharacteristic->notify();
  //    txValue++;
  //    delay(10); // bluetooth stack will go into congestion, if too many packets are sent
  //  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
