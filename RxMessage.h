#ifndef LRH_RXMESSAGE_H
#define LRH_RXMESSAGE_H

#include <Arduino.h>
#include "Message.h"

class RxMessage: public Message {

  public: 
    
    //variables
    float snr;
    float rssi;
    int freqOffset;

    //constructor
    RxMessage();
    RxMessage(String incoming);

    //functions
    void parseMessage(String incoming);
    
};

#endif
