#ifndef LRH_LORARADIO_H
#define LRH_LORARADIO_H

#include <SPI.h>              // include libraries
#include <LoRa.h>
#include "Message.h"
#include "RxMessage.h"
#include "Display.h"

#define LORA_CS_PIN 5 // 5 for v1.0; 2 for board v1.06
#define LORA_RESET_PIN 13
#define LORA_IRQ_PIN 35

class LoRaRadio {
  public:
    //constructor
    LoRaRadio(String call);
    LoRaRadio(String call, Display &disp);

    // functions
    void init();
    RxMessage* loop();
    void send(Message msg);
    void setDisplay(Display &disp);

  private:

    //functions
    RxMessage* parsePacket(int packetSize);
    void pollRx();

    //variables
    String call = "";
    unsigned int LoRaFreq           = 434100E3;  // default: 434100E3 // DB0ARD output: 433900E3
    unsigned int LoRaFreqTx         = 433775E3;  // DB0ARD input 433775E3; 
    bool         LoRaModeDirect     = true;      // don't change to Tx frequency
    const byte   LoRaSpreadingF     = 12;        // ranges from 6-12,default 7 see API docs
    const long   LoRaBw             = 125E3;     // Supported values are 7.8E3, 10.4E3, 15.6E3, 20.8E3, 31.25E3, 41.7E3, 62.5E3, 125E3, 250E3, and 500E3. defaults to 125E3
    const byte   LoRaCodingRate4    = 5;         // Supported values are between 5 and 8,defaults to 5
    const byte   LoRaPreambleLength = 8;         // Supported values are between 6 and 65535. defaults to 8
    const byte   LoRaSyncWord       = 0x12;      // *do not change* default sync word
    const byte   LoRaPower          = 14;        // power in dBm. Range: <=17, default: 10
    Display disp;
    RxMessage inMsg;

};
#endif
