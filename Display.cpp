#include "Display.h"

#define FIRSTLINE  16
#define FONTSIZE   7
#define LINEHEIGHT 8
#define STATUSAREA 40


Display::Display() : mydisplay(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET) {


}

void Display::displayTx(bool sending) {
  this->sending = sending;
  //  display.clearDisplay();
  // text display tests
  mydisplay.setTextSize(1);
  //  display.setTextColor(WHITE, BLACK);
  mydisplay.setCursor(0, 0);
  if (sending) {
    mydisplay.setTextColor(BLACK, WHITE); // 'inverted' text
    mydisplay.println("TX");
  } else {
    mydisplay.setTextColor(WHITE, BLACK);
    mydisplay.println("  ");
  }
  mydisplay.display();
}

void Display::displayTxMessage(Message msg) {
  mydisplay.clearDisplay();
  mydisplay.setTextColor(WHITE, BLACK);
  mydisplay.setTextSize(1);
  displayTx(false);
  mydisplay.setCursor(0, FIRSTLINE);
  mydisplay.println(msg.source + ": " + msg.message);
  mydisplay.setCursor(0, STATUSAREA);
//  mydisplay.println("Freq: " + String(msg.frequency / 1.0E6)); // + "M/P: " + String(LoRaPower));
  //  mydisplay.println("BW: " + String(LoRaBw / 1000) + "k/SF:" + String(LoRaSpreadingF) + "/CR:" + String(LoRaCodingRate4));
  mydisplay.display();
  //  Serial.println("Message displayed: " + msg.getFullString() + " (l: " + msg.getFullString().length() + ")");
}

void Display::displayRxMessage(RxMessage incoming) {
  mydisplay.clearDisplay();
  // text display tests
  mydisplay.setTextSize(1);
  //display.setTextColor(BLACK, WHITE); // 'inverted' text
  mydisplay.setTextColor(WHITE, BLACK);
  mydisplay.setCursor(10, 0);
  mydisplay.println("RX");

  //display.setTextColor(WHITE, BLACK);
  //  mydisplay.setCursor(10, 0);
  //  mydisplay.println("  ");

  mydisplay.setCursor(0, FIRSTLINE);
  mydisplay.println("Received: " + incoming.source + ": " + incoming.message);
  mydisplay.setCursor(0, STATUSAREA);
  mydisplay.println("RSSI: " + String(incoming.rssi));
  mydisplay.println("SNR: " + String(incoming.snr));
  mydisplay.println("Offset: " + String(incoming.freqOffset));


  Serial.println("Received: " + incoming.source + ": " + incoming.message);
  Serial.println("RSSI: " + String(incoming.rssi));
  Serial.println("Snr: " + String(incoming.snr));
  Serial.println("freqErr: " + String(incoming.freqOffset));

  mydisplay.display();
}

void Display::displayString(String in) {
  mydisplay.clearDisplay();
  mydisplay.setCursor(0, FIRSTLINE);
  mydisplay.println(in);
  mydisplay.display();
}

void Display::init() {

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!mydisplay.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    // for(;;); // Don't proceed, loop forever // continue if no OLED is used
  }

  mydisplay.cp437(true);         // Use full 256 char 'Code Page 437' fontv
  mydisplay.clearDisplay();
  mydisplay.setTextSize(1);
  mydisplay.setTextColor(WHITE, BLACK);
  mydisplay.setCursor(40, 1);
  mydisplay.println("LoRaHAM");
  mydisplay.setCursor(50, FIRSTLINE);
  mydisplay.println("on");
  mydisplay.setCursor(2, FIRSTLINE + 16);
  mydisplay.println(" Gualtherius LoRaHAM");
  mydisplay.setCursor(46, FIRSTLINE + 32);
  mydisplay.println("v1.0");
  mydisplay.display();
  Serial.println("Display initialized");
  delay(3000);
}
