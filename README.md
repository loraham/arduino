# Arduino code to send and receive messages in the LoRaHAM network

## Environment
* Gualtherius LoRaHAM
* Arduino IDE 1.8.12
* Arduino libraries as listed in the source code

## Protocol

The protocol is aligned with the JS8Call protocol published here: https://docs.google.com/document/d/159S4wqMUVdMA7qBgaSWmU-iDI4C9wd4CuWnetN68O9U

* Simple text messages: `[call]: CQ LoRaHAM`
* Calling all stations: `[call]: @ALLCALL Sporadic-E opening now!`
* Directed message: `[call]: [destcall] Hi! Good to read you again!`
* Forwarded message: `[call]: [repeatercall]>[destcall] Indirect Hello via repeatercall`

